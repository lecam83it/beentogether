import SwiftUI
import UIPilot

struct HeaderWidget: View {
  let title: String
  
  @EnvironmentObject var navigation: UIPilot<AppRoute>
  
  var body: some View {
    ZStack {
      HStack {
        Button {
          navigation.pop()
        } label: {
          HStack(spacing: 0) {
            Image("back")
              .resizable()
              .frame(width: 20, height: 20)
            Text("Back")
              .font(.itim(size: 18))
              .foregroundColor(.black)
              .padding(.vertical, 6)
          }
        }
        .padding(.vertical, 8)
        .fixedSize()
      }
      .frame(height: 56)
      .frame(maxWidth: .infinity, alignment: .leading)
      
      Text(title)
        .font(.itim(size: 24))
        .foregroundColor(.black)
        .frame(height: 40, alignment: .center)
        .padding(.horizontal, 8)
        .fixedSize()
    }
    .frame(height: 56)
    .frame(maxWidth: .infinity)
    .padding(.top, UIScreen.statusBarHeight)
    .padding(.horizontal, 12)
    .background(Color.white)
  }
}
