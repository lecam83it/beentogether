import Foundation
import Combine

class FDateController: ObservableObject {
  
  init(date: Date = Date()) {
    setTimeInFlipControllers(time: dateFormater.string(from: date))
  }
  
  private(set) lazy var flipControllers = { (0...7).map { _ in FlipController() } }()
  
  
  private func setTimeInFlipControllers(time: String) {
    zip(time, flipControllers).forEach { number, controller in
      controller.text = "\(number)"
    }
  }
  
  func updateTimer(date: Date) {
    setTimeInFlipControllers(time: dateFormater.string(from: date))
  }
  
  private var cancellables = Set<AnyCancellable>()
  
  private let dateFormater = DateFormatter.dateFormater
}
