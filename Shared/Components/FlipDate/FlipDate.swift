import SwiftUI

struct FlipDate: View {
  let size: CGFloat
  let isDark: Bool
  var clockController : FDateController
  
  init(
    size: CGFloat = 35,
    isDark: Bool = false,
    controller: FDateController = FDateController(date: Date())
  ) {
    self.size = size
    self.isDark = isDark
    self.clockController = controller
  }
  
  var body: some View {
    HStack(spacing: 6) {
      HStack(spacing: 2) {
        FlipView(size: size, isDark: isDark, controller: clockController.flipControllers[0])
        FlipView(size: size, isDark: isDark, controller: clockController.flipControllers[1])
      }
      
      HStack(spacing: 2) {
        FlipView(size: size, isDark: isDark, controller: clockController.flipControllers[2])
        FlipView(size: size, isDark: isDark, controller: clockController.flipControllers[3])
      }
      
      HStack(spacing: 2) {
        FlipView(size: size, isDark: isDark, controller: clockController.flipControllers[4])
        FlipView(size: size, isDark: isDark, controller: clockController.flipControllers[5])
        FlipView(size: size, isDark: isDark, controller: clockController.flipControllers[6])
        FlipView(size: size, isDark: isDark, controller: clockController.flipControllers[7])
      }
    }
  }
}
