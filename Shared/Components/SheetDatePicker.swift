import SwiftUI

struct SheetDatePicker: View {
  @Binding var isPresent: Bool
  @Binding var date: Date?
  @Binding var currentDate: Date
  let title: String
  init(
    isPresent visible: Binding<Bool>,
    date selectedDate: Binding<Date?>,
    currentDate: Binding<Date>,
    title: String = ""
  ) {
    self._isPresent = visible
    self._date = selectedDate
    self._currentDate = currentDate
    self.title = title
  }
  
  var body: some View {
    VStack(alignment: .center, spacing: 0) {
      HStack {
        Button {
          self.isPresent.toggle()
        } label: {
          Text("Cancel")
            .font(.itim(size: 16))
            .foregroundColor(.white)
            .padding(.horizontal, 8)
            .padding(.vertical, 4)
            .frame(height: 32)
            .background(Color.black.opacity(0.2))
            .cornerRadius(32)
        }
        
        Spacer()
        
        Text(title)
          .font(.itim(size: 20))
          .foregroundColor(.black.opacity(0.7))
          .padding(.horizontal, 8)
          .padding(.vertical, 4)
          .frame(height: 32)
        
        Spacer()
        
        Button {
          self.isPresent.toggle()
          self.date = self.currentDate
        } label: {
          Text("Confirm")
            .font(.itim(size: 16))
            .foregroundColor(.white)
            .padding(.horizontal, 8)
            .padding(.vertical, 4)
            .frame(height: 32)
            .background(Color.red.opacity(0.8))
            .cornerRadius(32)
        }
      }
      .padding(.horizontal, 20)
      
      DatePicker("", selection: $currentDate, displayedComponents: .date)
        .labelsHidden()
        .datePickerStyle(.wheel)
    }
  }
}
