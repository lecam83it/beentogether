import SwiftUI

struct FlipClock: View {
  let size: CGFloat
  let isDark: Bool
  let controller : ClockController
  
  init(
    size: CGFloat = 35,
    isDark: Bool = false,
    controller: ClockController = ClockController()
  ) {
    self.size = size
    self.isDark = isDark
    self.controller = controller
  }
  
  var body: some View {
    HStack(spacing: 6) {
      // hour
      HStack(spacing: 2) {
        FlipView(size: size, isDark: isDark, controller: controller.flipControllers[0])
        FlipView(size: size, isDark: isDark, controller: controller.flipControllers[1])
      }
      // minutes
      HStack(spacing: 2) {
        FlipView(size: size, isDark: isDark, controller: controller.flipControllers[2])
        FlipView(size: size, isDark: isDark, controller: controller.flipControllers[3])
      }
      // second
      HStack(spacing: 2) {
        FlipView(size: size, isDark: isDark, controller: controller.flipControllers[4])
        FlipView(size: size, isDark: isDark, controller: controller.flipControllers[5])
      }
    }
  }
}
