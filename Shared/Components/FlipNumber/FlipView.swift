import SwiftUI

struct FlipView: View {
  let size: CGFloat
  let isDark: Bool
  @ObservedObject var controller: FlipController
  
  init(
    size: CGFloat = 35,
    isDark: Bool = false,
    controller: FlipController
  ) {
    self.size = size
    self.isDark = isDark
    self.controller = controller
  }
  
  var body: some View {
    VStack(spacing: 0) {
      ZStack {
        SingleFlipText(controller.newValue, size: size, isDark: isDark, type: .top)
        SingleFlipText(controller.oldValue, size: size, isDark: isDark, type: .top)
          .rotation3DEffect(.degrees(controller.animationTop ? -89.99 : 0), axis: (1, 0, 0), anchor: .bottom, perspective: 1.0)
      }
      Color.clear.frame(height: 1)
      ZStack {
        SingleFlipText(controller.oldValue, size: size, isDark: isDark, type: .bottom)
        SingleFlipText(controller.newValue, size: size, isDark: isDark, type: .bottom)
          .rotation3DEffect(.degrees(controller.animationBottom ? 0 : 89.99), axis: (1, 0, 0), anchor: .top, perspective: 0.5)
      }
    }
    .fixedSize()
  }
}
