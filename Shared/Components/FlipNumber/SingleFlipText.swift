import SwiftUI

struct SingleFlipText: View {
  private let text: String
  private let size: CGFloat
  private let isDark: Bool
  private let type: FlippedType
  init(
    _ text: String,
    size: CGFloat = 50,
    isDark: Bool = false,
    type: FlippedType
  ) {
    self.text = text
    self.size = size
    self.type = type
    self.isDark = isDark
  }
  
  var body: some View {
    Text(text)
      .font(.itim(size: size))
      .foregroundColor(!isDark ? .white : .black)
      .fixedSize()
      .frame(width: size / 2, height: size, alignment: .center)
      .clipped()
      .frame(height: size / 2, alignment: type.alignment)
      .clipped()
      .padding([.leading, .trailing], size / 6)
      .background(isDark ? Color.white : Color.black54)
      .padding(type.padding, -0.5)
      .clipped()
  }
  
  enum FlippedType {
    case bottom
    case top
    
    var padding: Edge.Set {
      switch self {
      case .bottom:
        return .top
      case .top:
        return .bottom
      }
    }
    
    var alignment: Alignment {
      switch self {
      case .bottom:
        return .bottom
      case .top:
        return .top
      }
    }
  }
}
