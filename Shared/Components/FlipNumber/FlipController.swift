import Foundation
import SwiftUI

class FlipController: ObservableObject, Identifiable {
  
  @Published
  var oldValue: String = "0"
  
  @Published
  var newValue: String = "0"
  
  @Published
  var animationTop: Bool = false
  
  @Published
  var animationBottom: Bool = false
  
  var text: String = "" {
    didSet { updateUI(old: oldValue, new: text) }
  }
  
  func updateUI(old: String, new: String) {
    guard old != new else { return }
    self.oldValue = old
    animationTop = false
    animationBottom = false

    withAnimation(.easeOut(duration: 0.2)) { [weak self] in
      self?.newValue = new
      self?.animationTop = true
    }
    
    withAnimation(.easeOut(duration: 0.2).delay(0.2)) { [weak self] in
      self?.animationBottom = true
    }
  }
}
