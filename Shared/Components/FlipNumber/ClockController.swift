import Foundation
import Combine

class ClockController {
  
  init() {
    setTimeInFlipControllers(time: timeFormatter.string(from: Date()))
  }
  
  func updateTimer(date: Date) {
    setTimeInFlipControllers(time: timeFormatter.string(from: date))
  }
  
  private func setTimeInFlipControllers(time: String) {
    zip(time, flipControllers).forEach { number, controller in
      controller.text = "\(number)"
    }
  }
  
  private(set) lazy var flipControllers = { (0...5).map { _ in FlipController() } }()
  
  private let timeFormatter = DateFormatter.timeFormatter
}
