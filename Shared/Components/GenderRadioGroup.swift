import SwiftUI

struct GenderRadioGroup: View {
  let callback: (String) -> Void
  @State var selected: String
  
  init(selected: String, callback: @escaping(String) -> Void) {
    self.callback = callback
    self.selected = selected
  }
  var body: some View {
    Group {
      RadioButton(
        id: Gender.male.rawValue,
        size: 16,
        isSelected: selected == Gender.male.rawValue
      ) { value in
        selected = value
        self.callback(value)
      } content: {
        Image("male")
          .resizable()
          .frame(width: 18, height: 18)
      }
      
      RadioButton(
        id: Gender.female.rawValue,
        size: 16,
        isSelected: selected == Gender.female.rawValue
      ) { value in
        selected = value
        self.callback(value)
      } content: {
        Image("female")
          .resizable()
          .frame(width: 18, height: 18)
      }
    }
  }
}

