import SwiftUI

struct TakePhotoView: View {
  
  var onPressed: () -> Void
  @Binding var selectedImage: UIImage?
  
  var body: some View {
    Button {
      self.onPressed()
    } label: {
      if selectedImage == nil {
        ZStack {
          Circle().fill(Color.lightGray)
          Image("photo").resizable().frame(width: 32, height: 32)
        }
      } else {
        Image(uiImage: selectedImage!)
          .resizable()
          .clipShape(Circle())
      }
      
    }
  }
}
