import SwiftUI

struct ProgressWidget: View {
  let _kSecondsOfDate: Int = 86400
  let date: Date
  
  init(date: Date) {
    self.date = date
  }
  
  var body: some View {
    GeometryReader { geometry in
      ZStack(alignment: .leading) {
        RoundedRectangle(cornerRadius: 6)
          .fill(.black.opacity(0.1))
          .frame(width: geometry.size.width, height: 6)
        
        RoundedRectangle(cornerRadius: 6)
          .fill(.red)
          .frame(width: (date.timeInterval / Double(_kSecondsOfDate)) * geometry.size.width, height: 6)
      }
    }
    .frame(height: 6)
  }
}
