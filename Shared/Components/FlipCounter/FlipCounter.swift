import SwiftUI

struct FlipCounter: View {
  let size: CGFloat
  let isDark: Bool
  @ObservedObject var controller: FCounterController
  
  init(
    size: CGFloat = 35,
    isDark: Bool = false,
    controller: FCounterController
  ) {
    self.size = size
    self.isDark = isDark
    self.controller = controller
  }
  
  var body: some View {
    HStack(spacing: 2) {
      ForEach(controller.flipControllers) { flipController in
        FlipView(size: size, isDark: isDark, controller: flipController)
      }
    }
  }
}

