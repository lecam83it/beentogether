import Foundation
import Combine

class FCounterController: ObservableObject {
  var value: String
  
  init(value: String) {
    self.value = value
    initialize()
    setValueInFlipControllers(value: value)
  }
  
  @Published
  var flipControllers: [FlipController] = []
  
  func initialize() {
    for _ in 0..<(value.count) {
      flipControllers.append(FlipController())
    }
  }
  
  private func setValueInFlipControllers(value: String) {
    zip(value, flipControllers).forEach { number, controller in
      controller.text = "\(number)"
    }
  }
  
  func updateTimer(newValue: String) {
    if self.value.count != newValue.count {
      self.value = newValue
      flipControllers.append(FlipController())
    }
    setValueInFlipControllers(value: newValue)
  }
  
  private var cancellables = Set<AnyCancellable>()
}
