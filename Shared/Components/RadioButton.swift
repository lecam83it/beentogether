import SwiftUI

struct RadioButton<Content: View>: View {
  let id: String
  let size: CGFloat
  let color: Color
  let isSelected: Bool
  let callback: (String) -> Void
  let content: () -> Content
  
  public init(
    id: String,
    size: CGFloat = 24,
    color: Color = .black,
    isSelected: Bool = false,
    callback: @escaping (String) -> Void,
    @ViewBuilder content: @escaping () -> Content
  ) {
    self.id = id
    self.size = size
    self.color = color
    self.isSelected = isSelected
    self.callback = callback
    self.content = content
  }
  
  var body: some View {
    Button {
      self.callback(id)
    } label: {
      HStack(alignment: .center, spacing: 4) {
        Image(systemName: self.isSelected ? "largecircle.fill.circle" : "circle")
          .renderingMode(.original)
          .resizable()
          .aspectRatio(contentMode: .fit)
          .foregroundColor(color)
          .frame(width: self.size, height: self.size)
        
        self.content()
      }
      .padding(.horizontal, 8)
    }
  }
}
