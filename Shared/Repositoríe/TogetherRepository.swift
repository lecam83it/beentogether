import Foundation
import UIKit

class TogetherRepository {
  
  static let shared = TogetherRepository()
  
  let persistenceController = PersistenceController.shared
  let viewContext = PersistenceController.shared.container.viewContext
  
  private init() {}
  
  var you: Person? = nil
  var partner: Person? = nil
  var loveDate: Date?
  
  var yourImage: UIImage? = nil
  var partnerImage: UIImage? = nil
  
  var together: Together?
  
  var isNeedReloadHome: Bool = false
  
  func initialize() {
    you = Person(context: viewContext)
    you?.id = Date().milliseconds

    partner = Person(context: viewContext)
    partner?.id = Date().milliseconds + 1

    loveDate = Date()

    together = Together(context: viewContext)
  }
  
  func saveInfo() {
    together?.addToPersons([you!, partner!])
    together?.been = loveDate
    persistenceController.save()
  }
  
  func updateInfo() {
    isNeedReloadHome = true
    together?.been = loveDate
    persistenceController.save()
  }
  
  func getInfo(
    onComplete: @escaping() -> Void = {},
    onError: @escaping() -> Void = {}
  ) {
    persistenceController.fetch { together, error in
      if let error = error {
        print("Error: \(error.localizedDescription)")
        onError()
        return
      }
      
      guard let together = together else {
        print("Together nil")
        onError()
        return
      }
      
      guard let persons = together.persons, persons.count == 2 else {
        fatalError("No data to display")
      }
    
      DispatchQueue.main.async {
        self.together = together
        
        var personList = persons.allObjects
        personList = personList.sorted(by: { (p1, p2) in
          (p1 as! Person).id < (p2 as! Person).id
        })
        
        let first: Person = personList.first as! Person
        let second: Person = personList.last as! Person
        
        self.you = first
        self.partner = second
        
        self.yourImage = (first.avatar != nil) ? UIImage(data: first.avatar!) : nil
        self.partnerImage = (second.avatar != nil) ? UIImage(data: second.avatar!) : nil
        
        self.loveDate = together.been!
        onComplete()
      }
    }
  }
}
