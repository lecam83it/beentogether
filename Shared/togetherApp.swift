import SwiftUI
import UIPilot

enum AppRoute: Equatable {
  case Splash
  case Home
  case Setup
  case Menu
  case Edit
  case ClockThemes
}

class ThemesController: ObservableObject {
  var preferences = SharedPreferences.shared
  
  @Published
  var clockThemes: ClockType = .normal
  
  init() {
    self.clockThemes = preferences.clockThemes
  }
}

@main
struct togetherApp: App {
  
  @StateObject var pilot = UIPilot(initial: AppRoute.Splash)
  @StateObject var themes = ThemesController()
  
  var body: some Scene {
    WindowGroup {
      UIPilotHost(pilot)  { route in
        switch route {
        case .Splash:
          return AnyView(SplashView().navigationBarHidden(true))
        case .Setup:
          return AnyView(YourInfoView().navigationBarHidden(true))
        case .Edit:
          return AnyView(EditView().navigationBarHidden(true))
        case .Menu:
          return AnyView(MenuView().navigationBarHidden(true))
        case .ClockThemes:
          return AnyView(ClockThemesView().navigationBarHidden(true))
        case .Home:
          return AnyView(HomeView()
            .navigationBarHidden(true))
        }
      }
      .environmentObject(themes)
    }
  }
}
