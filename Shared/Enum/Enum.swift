import Foundation

enum Zodiac: String {
  case Aries = "Aries"
  case Taurus = "Taurus"
  case Gemini = "Gemini"
  case Cancer = "Cancer"
  case Leo = "Leo"
  case Virgo = "Virgo"
  case Libra = "Libra"
  case Scorpio = "Scorpio"
  case Sagittarius = "Sagittarius"
  case Capricorn = "Capricorn"
  case Aquarius = "Aquarius"
  case Pisces = "Pisces"
}


enum Gender: String {
  case male = "male"
  case female = "female"
}

enum ErrorType {
  case yourBirthdayInvalid
  case partnerBirthdayInvalid
  case loveDateInvalid
}

extension ErrorType {
  var description: String {
    switch self {
    case .yourBirthdayInvalid:
      return "Your Birthday cannot be older than current date"
    case .partnerBirthdayInvalid:
      return "Partner's Birthday cannot be older than current date"
    case .loveDateInvalid:
      return "Together Date cannot be older than current date"
    }
  }
}

