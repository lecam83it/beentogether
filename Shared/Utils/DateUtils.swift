import Foundation

struct DateUtils {
  
  static func getAge(birthday date: Date?) -> Int {
    guard let date = date else {
      return 0
    }
    
    let calendar = Calendar.current
    let births = calendar.dateComponents([.year, .month, .day], from: date)
    let currents = calendar.dateComponents([.year, .month, .day], from: Date())
    
    var age = currents.year! - births.year!
    let month = currents.month! - births.month!
    let day = currents.day! - births.day!
    if month < 0 || (month == 0 && day < 0) {
      age = age - 1
    }
    return age
  }
  
  static func getZodiac(birthday date: Date?) -> Zodiac? {
    guard let date = date else {
      return nil
    }
    
    let calendar = Calendar.current
    let births = calendar.dateComponents([.month, .day], from: date)
    
    guard let cMonth = births.month, let cDay = births.day else {
      return nil
    }
    
    if isAries(month: cMonth, day: cDay) {
      return .Aries
    }
    
    if isTaurus(month: cMonth, day: cDay) {
      return .Taurus
    }
    
    if isGemini(month: cMonth, day: cDay) {
      return .Gemini
    }
    
    if isCancer(month: cMonth, day: cDay) {
      return .Cancer
    }
    
    if isLeo(month: cMonth, day: cDay) {
      return .Leo
    }
    
    if isVirgo(month: cMonth, day: cDay) {
      return .Virgo
    }
    
    if isLibra(month: cMonth, day: cDay) {
      return .Libra
    }
    
    if isScorpio(month: cMonth, day: cDay) {
      return .Scorpio
    }
    
    if isSagittarius(month: cMonth, day: cDay) {
      return .Sagittarius
    }
    
    if isCapricorn(month: cMonth, day: cDay) {
      return .Capricorn
    }
    
    if isAquarius(month: cMonth, day: cDay) {
      return .Aquarius
    }
    
    if isPisces(month: cMonth, day: cDay) {
      return .Pisces
    }
    
    return nil
  }
  
  static func isAries(month: Int, day: Int) -> Bool {
    if month == 3 && day >= 21 {
      return true
    }
    
    if month == 4 && day <= 19 {
      return true
    }
    
    return false
  }
  
  static func isTaurus(month: Int, day: Int) -> Bool {
    if month == 4 && day >= 20 {
      return true
    }
    
    if month == 5 && day <= 20 {
      return true
    }
    
    return false
  }
  
  static func isGemini(month: Int, day: Int) -> Bool {
    if month == 5 && day >= 21 {
      return true
    }
    
    if month == 6 && day <= 20 {
      return true
    }
    
    return false
  }
  
  static func isCancer(month: Int, day: Int) -> Bool {
    if month == 6 && day >= 21 {
      return true
    }
    
    if month == 7 && day <= 22 {
      return true
    }
    
    return false
  }
  
  static func isLeo(month: Int, day: Int) -> Bool {
    if month == 7 && day >= 23 {
      return true
    }
    
    if month == 8 && day <= 22 {
      return true
    }
    
    return false
  }
  
  static func isVirgo(month: Int, day: Int) -> Bool {
    if month == 8 && day >= 23 {
      return true
    }
    
    if month == 9 && day <= 22 {
      return true
    }
    
    return false
  }
  
  static func isLibra(month: Int, day: Int) -> Bool {
    if month == 9 && day >= 23 {
      return true
    }
    
    if month == 10 && day <= 22 {
      return true
    }
    
    return false
  }
  
  static func isScorpio(month: Int, day: Int) -> Bool {
    if month == 10 && day >= 23 {
      return true
    }
    
    if month == 11 && day <= 21 {
      return true
    }
    
    return false
  }
  
  static func isSagittarius(month: Int, day: Int) -> Bool {
    if month == 11 && day >= 22 {
      return true
    }
    
    if month == 12 && day <= 21 {
      return true
    }
    
    return false
  }
  
  static func isCapricorn(month: Int, day: Int) -> Bool {
    if month == 12 && day >= 22 {
      return true
    }
    
    if month == 1 && day <= 19 {
      return true
    }
    
    return false
  }
  
  static func isAquarius(month: Int, day: Int) -> Bool {
    if month == 1 && day >= 20 {
      return true
    }
    
    if month == 2 && day <= 18 {
      return true
    }
    
    return false
  }
  
  static func isPisces(month: Int, day: Int) -> Bool {
    if month == 2 && day >= 19 {
      return true
    }
    
    if month == 3 && day <= 20 {
      return true
    }
    
    return false
  }
}
