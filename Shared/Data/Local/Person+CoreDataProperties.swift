//
//  Person+CoreDataProperties.swift
//  together
//
//  Created by CamL on 08/07/2022.
//
//

import Foundation
import CoreData


extension Person {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Person> {
        return NSFetchRequest<Person>(entityName: "Person")
    }

    @NSManaged public var avatar: Data?
    @NSManaged public var birthday: Date?
    @NSManaged public var name: String?
    @NSManaged public var gender: String?
    @NSManaged public var id: Int64
    @NSManaged public var together: Together?

}

extension Person : Identifiable {

}
