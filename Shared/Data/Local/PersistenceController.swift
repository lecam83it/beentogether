import CoreData

struct PersistenceController {
  
  static let shared = PersistenceController()
  
  let container: NSPersistentContainer
  
  private init() {
    container = NSPersistentContainer(name: "Together")
    container.loadPersistentStores { description, error in
      if let error = error {
        fatalError("Error: \(error.localizedDescription)")
      }
    }
  }
  
  func save(completion: @escaping (Error?) -> () = {_ in }) {
    let context = container.viewContext
    if context.hasChanges {
      do {
        try context.save()
        completion(nil)
      } catch {
        print("Error: \(error.localizedDescription)")
        completion(error)
      }
    }
  }
  
  func delete(_ object: NSManagedObject, completion: @escaping (Error?) -> () = {_ in }) {
    let context = container.viewContext
    context.delete(object)
    save(completion: completion)
  }
  
  func fetch(completion: @escaping(Together?, Error?) -> Void) {
    do {
      let results: [Together] = try container.viewContext.fetch(Together.fetchRequest())
      if results.count > 0 {
        print("Results: \(results.count)")
        completion(results.first, nil)
      } else {
        completion(nil, nil)
      }
    } catch {
      completion(nil, error)
    }
    
  }
}
