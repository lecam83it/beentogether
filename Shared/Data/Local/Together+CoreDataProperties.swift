//
//  Together+CoreDataProperties.swift
//  together
//
//  Created by CamL on 08/07/2022.
//
//

import Foundation
import CoreData


extension Together {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Together> {
        return NSFetchRequest<Together>(entityName: "Together")
    }

    @NSManaged public var been: Date?
    @NSManaged public var persons: NSSet?

}

// MARK: Generated accessors for persons
extension Together {

    @objc(addPersonsObject:)
    @NSManaged public func addToPersons(_ value: Person)

    @objc(removePersonsObject:)
    @NSManaged public func removeFromPersons(_ value: Person)

    @objc(addPersons:)
    @NSManaged public func addToPersons(_ values: NSSet)

    @objc(removePersons:)
    @NSManaged public func removeFromPersons(_ values: NSSet)

}

extension Together : Identifiable {

}
