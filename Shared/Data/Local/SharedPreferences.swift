import Foundation

enum PreferenceKeys: String {
  case kHasConfig = "kHasConfig"
  case kClockType = "kClockType"
}

struct SharedPreferences {
  static var shared = SharedPreferences()
  
  let preferences = UserDefaults.standard
  
  private init() {}
  
  var hasConfiguration: Bool {
    get {
      return preferences.bool(forKey: PreferenceKeys.kHasConfig.rawValue)
    }
    
    set(data) {
      preferences.set(data, forKey: PreferenceKeys.kHasConfig.rawValue)
    }
  }
  
  var clockThemes: ClockType {
    get {
      let result: String? = preferences.string(forKey: PreferenceKeys.kClockType.rawValue)
      guard let result = result else {
        return ClockType.normal
      }
      return ClockType(rawValue: result)!
    }
    
    set(data) {
      preferences.set(data.rawValue, forKey: PreferenceKeys.kClockType.rawValue)
    }
  }
}
