import Foundation

struct APIRequest {
  static let kBaseURL: String = "http://localhost:3000"
  
  static private func getHeaders() -> [String: String]? {
    return [
      "Content-Type": "application/json",
    ]
  }
  
  static private func getURL(from route: String) -> URL? {
    return URL(string: "\(kBaseURL)/\(route)")
  }
  
  static func get(
    _ api: String,
    params: [String: Any]? = nil,
    headers: [String: String]? = nil
  ) {
//    let finalHeaders = headers ?? getHeaders()
    let url = getURL(from: api)
    
    guard let url = url else {
      return
    }
    
    let task: URLSessionDataTask = URLSession.shared.dataTask(with: url) { data, response, error in
      if let _ = error {
        // handle error
        print("Client error!")
        return
      }
      
      guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
        print("Server error!")
        return
      }
      
      print("Parsing Data")
    }
    
    task.resume()
  }
}
