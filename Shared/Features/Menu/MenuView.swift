import SwiftUI
import UIPilot

struct MenuView: View {
  @EnvironmentObject var navigation: UIPilot<AppRoute>
  
  var body: some View {
    ZStack {
      Color.background.ignoresSafeArea()
      
      VStack {
        HeaderWidget(title: "Menu")
        VStack(spacing: 2) {
          RowButtonWidget("Edit Infomation") {
            navigation.push(.Edit)
          }
          
          RowButtonWidget("Change Clock Themes") {
            navigation.push(.ClockThemes)
          }
          
          RowButtonWidget("Change Background Themes") {
//            navigation.push(.ClockThemes)
          }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
      }
      .edgesIgnoringSafeArea([.top])
    }
  }
}

struct RowButtonWidget: View {
  let text: String
  let onPressed: () -> Void
  
  init(_ text: String, onPressed: @escaping() -> Void) {
    self.text = text
    self.onPressed = onPressed
  }
  
  var body: some View {
    Button {
      onPressed()
    } label: {
      Text(text)
        .font(.itim(size: 18))
        .foregroundColor(.black)
        .padding(.horizontal, 16)
        .frame(maxWidth: .infinity, alignment: .leading)
        .frame(height: 48)
        .background(Color.white)
    }
    
  }
}

struct MenuView_Previews: PreviewProvider {
  static var previews: some View {
    MenuView()
  }
}
