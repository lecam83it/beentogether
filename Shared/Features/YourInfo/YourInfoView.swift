import SwiftUI
import BottomSheet
import UIPilot

struct YourInfoView: View {
  @EnvironmentObject var navigation: UIPilot<AppRoute>
  
  @StateObject private var viewController = InfoViewController()
  @StateObject private var viewModel = YourInfoViewModel(
    repository: TogetherRepository.shared
  )
  @State private var currentDate: Date = Date()
  
  var body: some View {
    ZStack {
      Color.background.ignoresSafeArea()
      
      VStack(spacing: 0) {
        HStack(alignment: .center) {
          Text("Start")
            .font(.itim(size: 18))
            .foregroundColor(.white)
            .frame(height: 40)
            .padding(.vertical, 6)
            .padding(.horizontal, 12)
            .hidden()
          
          Spacer()
          
          Text("Set up")
            .font(.itim(size: 24))
            .foregroundColor(.black)
            .frame(height: 40, alignment: .center)
            .padding(.horizontal, 8)
          
          Spacer()
          
         
          Button {
            viewModel.saveInfo {
              navigation.push(.Home)
            } onError: { errorType in
              viewController.errorMessage = errorType.description
              viewController.visibleErrorAlert = true
            }
          } label: {
            Text("Start")
              .font(.itim(size: 18))
              .foregroundColor(.white)
              .padding(.vertical, 6)
              .padding(.horizontal, 12)
              .background(disabledFinishButton() ? Color.lightGray : Color.button)
              .cornerRadius(40)
          }
          .padding(.vertical, 8)
          .disabled(disabledFinishButton())
          .alert(isPresented: $viewController.visibleErrorAlert) {
            Alert(
              title: Text("Invalid Data"),
              message: Text("\(viewController.errorMessage)"),
              dismissButton: .default(Text("OK"))
            )
          }
        }
        .padding(.top, UIScreen.statusBarHeight)
        .frame(height: 56 + UIScreen.statusBarHeight)
        .padding(.horizontal, 20)
        .frame(maxWidth: .infinity)
        .padding(.horizontal, 0)
        .background(Color.white)
        
        Group {
          ScrollView(.vertical, showsIndicators: false) {
            VStack(spacing: 0) {
              
              HStack {
                Text("Your infomation")
                  .font(.itim(size: 22))
                  .foregroundColor(.black)
                
              }
              .frame(maxWidth: .infinity, alignment: .leading)
              .padding(.horizontal, 12)
              .padding(.vertical, 8)
              
              HStack(spacing: 20) {
                TakePhotoView(
                  onPressed: {
                    self.viewController.visibleYourImagePicker = true
                  },
                  selectedImage: $viewModel.yourImage
                )
                .frame(width: 68, height: 68, alignment: .center)
                
                
                VStack(spacing: 2) {
                  TextField("", text: $viewModel.yourName)
                    .disableAutocorrection(true)
                    .font(.itim(size: 16))
                    .foregroundColor(.black)
                    .placeholder(when: viewModel.yourName.isEmpty) {
                      Text("Entering your name...")
                        .font(.itim(size: 18))
                        .foregroundColor(.lightGray)
                        .padding(.horizontal, 4)
                    }
                  
                  Divider().frame(height: 1)
                    .background(Color.lightGray)
                  
                }
              }
              .padding(.horizontal, 12)
              .padding(.vertical, 8)
              
              HStack {
                HStack(spacing: 0) {
                  Text("Gender")
                    .font(.itim(size: 18))
                    .foregroundColor(.black)
                  
                  Text("*")
                    .font(.itim(size: 18))
                    .foregroundColor(.red)
                }
                
                Spacer()
                
                GenderRadioGroup(selected: viewModel.yourGender) { genderString in
                  viewModel.yourGender = genderString
                  UIApplication.shared.endEditing()
                }
              }
              .padding(.horizontal, 12)
              .padding(.vertical, 8)
              
              HStack {
                HStack(spacing: 0) {
                  Text("Date of Birth")
                    .font(.itim(size: 18))
                    .foregroundColor(.black)
                  
                  Text("*")
                    .font(.itim(size: 18))
                    .foregroundColor(.red)
                }
                
                Spacer()
                
                Button {
                  UIApplication.shared.endEditing()
                  
                  self.viewController.visibleYourDatePicker.toggle()
                  self.currentDate = viewModel.yourBirthday ?? Date()
                } label: {
                  Image("date")
                    .resizable()
                    .frame(width: 20, height: 20)
                  
                  if let yourBirthday = viewModel.yourBirthday {
                    Text(yourBirthday.toDateString(with: "dd/MM/yyyy"))
                      .font(.itim(size: 18))
                      .foregroundColor(.black)
                  }
                }
                
              }
              .padding(.horizontal, 12)
              .padding(.vertical, 8)
            }
            .padding(.all, 8)
            .background(Color.white)
            .cornerRadius(8)
            .padding(.horizontal, 16)
            .padding(.top, 16)
            
            
            VStack(spacing: 0) {
              HStack {
                Text("Partner's infomation")
                  .font(.itim(size: 22))
                  .foregroundColor(.black)
                
              }
              .frame(maxWidth: .infinity, alignment: .leading)
              .padding(.horizontal, 12)
              .padding(.vertical, 8)
              
              HStack(spacing: 20) {
                TakePhotoView(
                  onPressed: {
                    self.viewController.visiblePartnerImagePicker = true
                  },
                  selectedImage: $viewModel.partnerImage
                )
                .frame(width: 68, height: 68, alignment: .center)
                
                
                VStack(spacing: 2) {
                  TextField("", text: $viewModel.partnerName)
                    .disableAutocorrection(true)
                    .font(.itim(size: 16))
                    .foregroundColor(.black)
                    .placeholder(when: viewModel.partnerName.isEmpty) {
                      Text("Entering partner's name...")
                        .font(.itim(size: 18))
                        .foregroundColor(.lightGray)
                        .padding(.horizontal, 4)
                    }
                  
                  Divider().frame(height: 1)
                    .background(Color.lightGray)
                }
              }
              .padding(.horizontal, 12)
              .padding(.vertical, 8)
              
              HStack {
                HStack(spacing: 0) {
                  Text("Gender")
                    .font(.itim(size: 18))
                    .foregroundColor(.black)
                  
                  Text("*")
                    .font(.itim(size: 18))
                    .foregroundColor(.red)
                }
                
                Spacer()
                
                GenderRadioGroup(selected: viewModel.partnerGender) { genderString in
                  viewModel.partnerGender = genderString
                  UIApplication.shared.endEditing()
                }
              }
              .padding(.horizontal, 12)
              .padding(.vertical, 8)
              
              HStack {
                HStack(spacing: 0) {
                  Text("Date of Birth")
                    .font(.itim(size: 18))
                    .foregroundColor(.black)
                  
                  Text("*")
                    .font(.itim(size: 18))
                    .foregroundColor(.red)
                }
                
                Spacer()
                
                Button {
                  UIApplication.shared.endEditing()
                  
                  self.viewController.visiblePartnerDatePicker.toggle()
                  self.currentDate = viewModel.partnerBirthday ?? Date()
                } label: {
                  Image("date")
                    .resizable()
                    .frame(width: 20, height: 20)
                  
                  if let partnerBirthday = viewModel.partnerBirthday {
                    Text(partnerBirthday.toDateString(with: "dd/MM/yyyy"))
                      .font(.itim(size: 18))
                      .foregroundColor(.black)
                  }
                }
              }
              .padding(.horizontal, 12)
              .padding(.vertical, 8)
            }
            .padding(.all, 8)
            .background(Color.white)
            .cornerRadius(8)
            .padding(.horizontal, 16)
            .padding(.top, 16)
            
            
            VStack(spacing: 0) {
              HStack {
                Text("Been together")
                  .font(.itim(size: 22))
                  .foregroundColor(.black)
                
              }
              .frame(maxWidth: .infinity, alignment: .leading)
              .padding(.horizontal, 12)
              .padding(.vertical, 8)
              
              HStack {
                HStack(spacing: 0) {
                  Text("Date Together")
                    .font(.itim(size: 18))
                    .foregroundColor(.black)
                  
                  Text("*")
                    .font(.itim(size: 18))
                    .foregroundColor(.red)
                }
                
                Spacer()
                
                Button {
                  UIApplication.shared.endEditing()
                  
                  self.viewController.visibleDateTogetherPicker.toggle()
                  self.currentDate = viewModel.togetherDate ?? Date()
                } label: {
                  Image("date")
                    .resizable()
                    .frame(width: 20, height: 20)
                  
                  if let togetherDate = viewModel.togetherDate {
                    Text(togetherDate.toDateString(with: "dd/MM/yyyy"))
                      .font(.itim(size: 18))
                      .foregroundColor(.black)
                  }
                }
                
              }
              .padding(.horizontal, 12)
              .padding(.vertical, 8)
            }
            .padding(.all, 8)
            .background(Color.white)
            .cornerRadius(8)
            .padding(.horizontal, 16)
            .padding(.top, 16)
          }
        }
      }
      .edgesIgnoringSafeArea([.top])
    }
    .onTapGesture {
      UIApplication.shared.endEditing()
    }
    .sheet(isPresented: $viewController.visibleYourImagePicker) {
      ImagePicker(selectedImage: $viewModel.yourImage, source: .photoLibrary)
    }
    .sheet(isPresented: $viewController.visiblePartnerImagePicker) {
      ImagePicker(selectedImage: $viewModel.partnerImage, source: .photoLibrary)
    }
    .bottomSheet(isPresented: $viewController.visibleYourDatePicker, height: 300) {
      SheetDatePicker(
        isPresent: $viewController.visibleYourDatePicker,
        date: $viewModel.yourBirthday,
        currentDate: $currentDate,
        title: "Date of Birth"
      )
    }
    .bottomSheet(isPresented: $viewController.visiblePartnerDatePicker, height: 300) {
      SheetDatePicker(
        isPresent: $viewController.visiblePartnerDatePicker,
        date: $viewModel.partnerBirthday,
        currentDate: $currentDate,
        title: "Date of Birth"
      )
    }
    .bottomSheet(isPresented: $viewController.visibleDateTogetherPicker, height: 300) {
      SheetDatePicker(
        isPresent: $viewController.visibleDateTogetherPicker,
        date: $viewModel.togetherDate,
        currentDate: $currentDate,
        title: "Date Together"
      )
    }
  }
  
  func disabledFinishButton() -> Bool {
    return viewModel.yourName.isEmpty || viewModel.yourBirthday == nil
    || viewModel.partnerName.isEmpty || viewModel.partnerBirthday == nil
    || viewModel.togetherDate == nil
  }
  
}
