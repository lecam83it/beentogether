import Foundation

enum ClockType: String {
  case normal = "normal"
  case flip = "flip"
}

class ClockThemesViewModel: BaseViewModel {
  
  let flipClockController: ClockController = ClockController()
  let flipDateController: FDateController = FDateController()
  @Published var flipCounterController: FCounterController
  var sharedPreferences = SharedPreferences.shared
  override init() {
    self.loveDate = repository.loveDate!
    let diffDate = (Date().diff(date: repository.loveDate!) ?? 0) + 1;
    self.dateCounter = diffDate
    self.flipCounterController = FCounterController(value: "\(diffDate)")
    self.selectedType = sharedPreferences.clockThemes
  }
  
  @Published
  var selectedType: ClockType = .normal
  
  let repository: TogetherRepository = TogetherRepository.shared
  
  @Published
  var currentDate: Date = Date()
  
  func setCurrentDate(date: Date) -> Void {
    currentDate = date
    self.dateCounter = (currentDate.diff(date: repository.loveDate!) ?? 0) + 1
    flipClockController.updateTimer(date: currentDate)
    flipDateController.updateTimer(date: currentDate)
    flipCounterController.updateTimer(newValue: "\(dateCounter)")
  }
  
  @Published
  var dateCounter: Int = 0
  
  @Published
  var loveDate: Date = Date()
  
  func initialize() {
    self.loveDate = repository.loveDate!
    self.dateCounter = (Date().diff(date: repository.loveDate!) ?? 0) + 1
    self.selectedType = sharedPreferences.clockThemes
  }
  
  func onTypeChange(newType: ClockType) {
    if selectedType == newType { return }
    self.selectedType = newType
    sharedPreferences.clockThemes = newType
  }
}
