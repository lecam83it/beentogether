import SwiftUI
import UIPilot

struct ClockThemesView: View {
  
  @StateObject
  private var themeViewModel: ClockThemesViewModel = ClockThemesViewModel()
  
  @EnvironmentObject
  var themes: ThemesController
  
  var body: some View {
    ZStack {
      Color.background.ignoresSafeArea()
      
      VStack {
        HeaderWidget(title: "Clock Themes")
        
        ScrollView(.vertical, showsIndicators: false) {
          NormalClockWidget(selectedType: $themeViewModel.selectedType, viewModel: themeViewModel) {
            themeViewModel.onTypeChange(newType: .normal)
            themes.clockThemes = .normal
          }
          
          FlipClockWidget(selectedType: $themeViewModel.selectedType, viewModel: themeViewModel) {
            themeViewModel.onTypeChange(newType: .flip)
            themes.clockThemes = .flip
          }
          
        }
      }
      .edgesIgnoringSafeArea([.top])
    }
    .onReceive(SharedTimer.shared.time) { value in
      themeViewModel.setCurrentDate(date: value)
    }
  }
}


struct NormalClockWidget: View {
  @Binding
  var selectedType: ClockType
  let onPressed: () -> Void
  @ObservedObject var viewModel: ClockThemesViewModel
  
  init(
    selectedType type: Binding<ClockType>,
    viewModel: ClockThemesViewModel,
    onPressed: @escaping () -> Void
  ) {
    self.onPressed = onPressed
    self._selectedType = type
    self.viewModel = viewModel
  }
  
  var body: some View {
    VStack(spacing: 2) {
      VStack(spacing: 4) {
        HStack {
          Text("\(viewModel.dateCounter)")
            .font(.itim(size: 48))
            .foregroundColor(.black)
          Text("days")
            .font(.itim(size: 48))
            .foregroundColor(.black)
        }
        
        HStack {
          Text("Dates together:")
            .font(.itim(size: 18))
            .foregroundColor(.black)
          
          Text("\(viewModel.loveDate.toDateString())")
            .font(.itim(size: 18))
            .foregroundColor(.black)
        }
        
        VStack(spacing: 4) {
          HStack {
            Text("\(viewModel.currentDate.toDateString())")
              .font(.itim(size: 18))
              .foregroundColor(.black)
            
            Spacer()
            Text("\(viewModel.currentDate.toTimeString())")
              .font(.itim(size: 18))
              .foregroundColor(.black)
          }
          ProgressWidget(date: viewModel.currentDate)
        }
        .padding(.top, 40)
      }
      .frame(maxWidth: .infinity)
      .padding()
      .background(Color.white)
      .cornerRadius(4)
      .overlay(selectedOverlay)
    }
    .padding(.horizontal, 16)
    .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
    .onTapGesture {
      onPressed()
    }
  }
  
  @ViewBuilder private var selectedOverlay: some View {
    if self.selectedType == .normal {
      RoundedRectangle(cornerRadius: 4).stroke(.blue, lineWidth: 2)
    }
  }
}


struct FlipClockWidget: View {
  @Binding
  var selectedType: ClockType
  let viewModel: ClockThemesViewModel
  let onPressed: () -> Void
  
  init(
    selectedType type: Binding<ClockType>,
    viewModel: ClockThemesViewModel,
    onPressed: @escaping () -> Void
  ) {
    self.onPressed = onPressed
    self.viewModel = viewModel
    self._selectedType = type
  }
  
  var body: some View {
    VStack(spacing: 2) {
      VStack(spacing: 4) {
        HStack {
          FlipCounter(size: 48, controller: viewModel.flipCounterController)
          Text("days")
            .font(.itim(size: 48))
            .foregroundColor(.black)
        }
        
        HStack {
          Text("Dates together:")
            .font(.itim(size: 18))
            .foregroundColor(.black)
          
          FlipDate(size: 18, controller: FDateController(date: viewModel.loveDate))
        }
        
        VStack(spacing: 4) {
          HStack {
            FlipDate(size: 18, controller: viewModel.flipDateController)
            Spacer()
            FlipClock(size: 18, controller: viewModel.flipClockController)
          }
          ProgressWidget(date: viewModel.currentDate)
        }
        .padding(.top, 40)
      }
      .frame(maxWidth: .infinity)
      .padding()
      .background(Color.white)
      .cornerRadius(4)
      .overlay(selectedOverlay)
    }
    .padding(.horizontal, 16)
    .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
    .onTapGesture {
      onPressed()
    }
    .onAppear {
      viewModel.initialize()
    }
  }
  
  @ViewBuilder private var selectedOverlay: some View {
    if self.selectedType == .flip {
      RoundedRectangle(cornerRadius: 4).stroke(.blue, lineWidth: 2)
    }
  }
}
