import Foundation
import UIKit

class EditController: ObservableObject {
  @Published var visibleYourImagePicker: Bool = false
  @Published var visiblePartnerImagePicker: Bool = false
  
  @Published var visibleYourDatePicker: Bool = false
  @Published var visiblePartnerDatePicker: Bool = false
  
  @Published var visibleDateTogetherPicker: Bool = false
  
  var errorMessage: String = ""
  @Published var visibleErrorAlert: Bool = false
}

class EditViewModel: BaseViewModel {
  let repository: TogetherRepository
  var sharedPreferences = SharedPreferences.shared
  
  
  @Published var yourImage: UIImage? = nil
  @Published var partnerImage: UIImage? = nil
  
  @Published var yourName: String = ""
  @Published var partnerName: String = ""
  
  @Published var yourBirthday: Date? = nil
  @Published var partnerBirthday: Date? = nil
  @Published var togetherDate: Date? = nil
  
  @Published var yourGender: String = Gender.male.rawValue
  @Published var partnerGender: String = Gender.female.rawValue
  
  public init(repository togetherRepository: TogetherRepository) {
    self.repository = togetherRepository
    let you: Person = togetherRepository.you!
    let partner: Person = togetherRepository.partner!
    
    yourName = you.name!
    yourGender = you.gender!
    yourBirthday = you.birthday!
    yourImage = togetherRepository.yourImage
    
    partnerName = partner.name!
    partnerGender = partner.gender!
    partnerBirthday = partner.birthday!
    partnerImage = togetherRepository.partnerImage
   
    togetherDate = togetherRepository.loveDate
  }
  
  func updateInfo(
    onSuccess: () -> Void,
    onError: @escaping(ErrorType) -> Void
  ) {
  
    if yourBirthday! > Date() {
      onError(.yourBirthdayInvalid)
      return
    }
    
    if partnerBirthday! > Date() {
      onError(.partnerBirthdayInvalid)
      return
    }
    
    if togetherDate! > Date() {
      onError(.loveDateInvalid)
      return
    }
    
    TogetherRepository.shared.you?.name = yourName
    TogetherRepository.shared.you?.birthday = yourBirthday
    TogetherRepository.shared.you?.avatar = yourImage?.pngData()
    TogetherRepository.shared.yourImage = yourImage
    TogetherRepository.shared.you?.gender = yourGender
    
    TogetherRepository.shared.partner?.name = partnerName
    TogetherRepository.shared.partner?.birthday = partnerBirthday
    TogetherRepository.shared.partner?.avatar = partnerImage?.pngData()
    TogetherRepository.shared.partnerImage = partnerImage
    TogetherRepository.shared.partner?.gender = partnerGender
    
    TogetherRepository.shared.loveDate = togetherDate!
    
    TogetherRepository.shared.updateInfo()
    
    onSuccess()
  }
}
