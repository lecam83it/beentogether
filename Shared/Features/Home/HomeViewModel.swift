import SwiftUI
import Combine

class HomeController: ObservableObject {
  @Published
  var heartScale: CGFloat = 1.0
}

class HomeViewModel: BaseViewModel {
  
  private let cancellable: AnyCancellable?
  
  let repository: TogetherRepository
  
  public let timer = Timer.publish(every: 1.0, on: .current, in: .default)
  
  var preferences: SharedPreferences = SharedPreferences.shared
  
  @Published
  var flipCounterController: FCounterController
  let flipDateController: FDateController
  let flipClockController: ClockController
  @Published
  var togetherDate: Date
  
  init(repository: TogetherRepository) {
    self.repository = repository
    self.cancellable = self.timer.connect() as? AnyCancellable
    self.togetherDate = repository.loveDate!
    
    let diffDate = (Date().diff(date: repository.loveDate!) ?? 0) + 1;
    self.dateCounter = diffDate
    self.flipCounterController = FCounterController(value: "\(diffDate)")
    self.flipDateController = FDateController()
    self.flipClockController = ClockController()
    
    let you: Person = repository.you!
    let partner: Person = repository.partner!
    
    self.yourName = you.name
    self.yourAvatar = (you.avatar != nil) ? UIImage(data: you.avatar!) : nil
    self.partnerAvatar =  (partner.avatar != nil) ? UIImage(data: partner.avatar!) : nil
    
    self.partnerName = partner.name
    
    self.yourAge = DateUtils.getAge(birthday: you.birthday)
    self.partnerAge = DateUtils.getAge(birthday: partner.birthday)
    
    self.yourZodiac = DateUtils.getZodiac(birthday: you.birthday)
    self.partnerZodiac = DateUtils.getZodiac(birthday: partner.birthday)
    
    self.yourGender = you.gender ?? Gender.male.rawValue
    self.partnerGender = partner.gender ?? Gender.female.rawValue
  }
  
  func reinitialize() {
    if !repository.isNeedReloadHome {
      return
    }
    repository.isNeedReloadHome = false
    
    self.togetherDate = repository.loveDate!
    self.dateCounter = (Date().diff(date: repository.loveDate!) ?? 0) + 1
    let you: Person = repository.you!
    let partner: Person = repository.partner!
    
    self.yourName = you.name
    self.yourAvatar = (you.avatar != nil) ? UIImage(data: you.avatar!) : nil
    self.partnerAvatar =  (partner.avatar != nil) ? UIImage(data: partner.avatar!) : nil
    
    self.partnerName = partner.name
    
    self.yourAge = DateUtils.getAge(birthday: you.birthday)
    self.partnerAge = DateUtils.getAge(birthday: partner.birthday)
    
    self.yourZodiac = DateUtils.getZodiac(birthday: you.birthday)
    self.partnerZodiac = DateUtils.getZodiac(birthday: partner.birthday)
    
    self.yourGender = you.gender ?? Gender.male.rawValue
    self.partnerGender = partner.gender ?? Gender.female.rawValue
  }
  
  @Published
  var currentDate: Date = Date()
  
  func setCurrentDate(date: Date) -> Void {
    currentDate = date
    self.dateCounter = (currentDate.diff(date: repository.loveDate!) ?? 0) + 1
    flipClockController.updateTimer(date: currentDate)
    flipDateController.updateTimer(date: currentDate)
    flipCounterController.updateTimer(newValue: "\(dateCounter)")
  }
  
  @Published
  var yourName: String? = nil
  
  @Published
  var yourAvatar: UIImage? = nil
  
  @Published
  var yourAge: Int = 0
  
  @Published
  var yourGender: String
  
  @Published
  var yourZodiac: Zodiac? = nil
  
  @Published
  var partnerName: String? = nil
  
  @Published
  var partnerAvatar: UIImage? = nil
  
  @Published
  var partnerAge: Int = 0
  
  @Published
  var partnerZodiac: Zodiac? = nil
  
  @Published
  var partnerGender: String
  
  @Published
  var dateCounter: Int = 0
  
  deinit {
    self.cancellable?.cancel()
  }
}
