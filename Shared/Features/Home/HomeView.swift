import SwiftUI
import UIPilot

struct HomeView: View {
  @ObservedObject var viewModel: HomeViewModel
  @StateObject var homeController = HomeController()
  
  init() {
    viewModel = HomeViewModel(repository: TogetherRepository.shared)
  }
  
  var body: some View {
    ZStack {
      TogetherImageBackground()
        .ignoresSafeArea()
      
      VStack {
        NavigationBarWidget()
        Spacer()
        TogetherInfomation(viewModel: viewModel)
        Spacer()
        CoupleInformation(viewModel: viewModel, controller: homeController)
        Spacer()
        DateTimeWidget(viewModel: viewModel)
      }
      .frame(width: UIScreen.width)
    }
    .onAppear {
      viewModel.reinitialize()
    }
  }
}

struct NavigationBarWidget: View {
  @EnvironmentObject var navigation: UIPilot<AppRoute>
  var body: some View {
    HStack {
      Spacer()
      
      Button {
        navigation.push(.Menu)
      } label: {
        Image("setting")
          .resizable()
          .frame(width: 24, height: 24)
          .foregroundColor(.white)
      }
    }
    .padding(.horizontal, 20)
    .frame(width: UIScreen.width, height: 56)
  }
}

struct TogetherInfomation: View {
  @ObservedObject var viewModel: HomeViewModel
  
  var body: some View {
    VStack(spacing: 2) {
      Text("Been Together")
        .font(.itim(size: 24))
        .foregroundColor(.white)
      DateCounterTogetherWidget(viewModel: viewModel)
      DateTogetherWidget(viewModel: viewModel)
    }
  }
}

struct DateCounterTogetherWidget: View {
  @ObservedObject var viewModel: HomeViewModel
  @EnvironmentObject var themes: ThemesController
  var body: some View {
    HStack {
      if themes.clockThemes == .normal {
        Text("\(viewModel.dateCounter)")
          .font(.itim(size: 48))
          .foregroundColor(.white)
      } else if themes.clockThemes == .flip {
        FlipCounter(size: 48, controller: viewModel.flipCounterController)
      }
      Text("days")
        .font(.itim(size: 48))
        .foregroundColor(.white)
    }
  }
}


struct DateTogetherWidget: View {
  @ObservedObject var viewModel: HomeViewModel
  @EnvironmentObject var themes: ThemesController
  var body: some View {
    HStack {
      Text("Dates together:")
        .font(.itim(size: 18))
        .foregroundColor(.white)
      if themes.clockThemes == .normal {
        Text("\(viewModel.togetherDate.toDateString())")
          .font(.itim(size: 18))
          .foregroundColor(.white)
      } else if themes.clockThemes == .flip {
        FlipDate(
          size: 18,
          controller: FDateController(date: viewModel.togetherDate)
        )
      }
    }
  }
}

struct CoupleInformation: View {
  @ObservedObject var viewModel: HomeViewModel
  @ObservedObject var controller: HomeController
  
  var body: some View {
    HStack(spacing: 0) {
      // Person One
      VStack(spacing: 0) {
        
        if viewModel.yourAvatar != nil {
          Image(uiImage: viewModel.yourAvatar!)
            .resizable()
            .frame(width: 68, height: 68)
            .cornerRadius(68)
            .overlay(Circle().stroke(.white, lineWidth: 2))
        } else {
          if viewModel.yourGender == Gender.male.rawValue {
            Circle().fill(.white)
              .frame(width: 68, height: 68)
              .overlay(Image("boy").resizable().frame(width: 52, height: 52))
          } else {
            Circle().fill(.white)
              .frame(width: 68, height: 68)
              .overlay(Image("girl").resizable().frame(width: 52, height: 52))
          }
        }
        
        Text(viewModel.yourName ?? "Boy")
          .font(.itim(size: 18))
          .foregroundColor(.white)
          .padding(.top, 8)
        
        HStack {
          HStack(alignment: .bottom, spacing: 2) {
            if viewModel.yourGender == Gender.male.rawValue {
              Image("male")
                .resizable()
                .frame(width: 12, height: 12)
            } else {
              Image("female")
                .resizable()
                .frame(width: 12, height: 12)
            }
            
            Text("\(viewModel.yourAge)")
              .font(.itim(size: 12))
              .foregroundColor(.black)
              .frame(height: 12)
          }
          .padding(.horizontal, 4)
          .padding(.vertical, 2)
          .background(Color.male)
          .cornerRadius(12)
          
          if viewModel.yourZodiac != nil {
            HStack(alignment: .bottom, spacing: 2) {
              ZodiacIcon(viewModel.yourZodiac!)
              
              Text("\(viewModel.yourZodiac!.rawValue)")
                .font(.itim(size: 12))
                .foregroundColor(.black)
                .frame(height: 12)
            }
            .padding(.horizontal, 4)
            .padding(.vertical, 2)
            .background(getZodiacColor(viewModel.yourZodiac!))
            .cornerRadius(12)
          }
        }
        .padding(.top, 4)
      }
      .frame(maxWidth: .infinity)
      .padding(.leading, 20)
      .padding(.trailing, 8)
      
      Image("heart")
        .resizable()
        .frame(width: 40, height: 40)
        .scaleEffect(controller.heartScale)
        .animation(Animation.easeInOut(duration: 0.8).repeatForever(), value: controller.heartScale)
        .onAppear {
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            startHeartAnimations()
          }
        }
      
      // Person Two
      VStack(spacing: 0) {
        if viewModel.partnerAvatar != nil {
          Image(uiImage: viewModel.partnerAvatar!)
            .resizable()
            .frame(width: 68, height: 68)
            .cornerRadius(68)
            .overlay(Circle().stroke(.white, lineWidth: 2))
        } else {
          if viewModel.partnerGender == Gender.male.rawValue {
            Circle().fill(.white)
              .frame(width: 68, height: 68)
              .overlay(Image("boy").resizable().frame(width: 52, height: 52))
          } else {
            Circle().fill(.white)
              .frame(width: 68, height: 68)
              .overlay(Image("girl").resizable().frame(width: 52, height: 52))
          }
        }
        
        Text(viewModel.partnerName ?? "Girl")
          .font(.itim(size: 18))
          .foregroundColor(.white)
          .padding(.top, 8)
        
        HStack {
          HStack(alignment: .bottom, spacing: 2) {
            if viewModel.yourGender == Gender.male.rawValue {
              Image("male")
                .resizable()
                .frame(width: 12, height: 12)
            } else {
              Image("female")
                .resizable()
                .frame(width: 12, height: 12)
            }
            
            
            Text("\(viewModel.partnerAge)")
              .font(.itim(size: 12))
              .foregroundColor(.black)
              .frame(height: 12)
          }
          .padding(.horizontal, 4)
          .padding(.vertical, 2)
          .background(Color.female)
          .cornerRadius(12)
          
          if viewModel.partnerZodiac != nil {
            HStack(alignment: .bottom, spacing: 2) {
              ZodiacIcon(viewModel.partnerZodiac!)
              
              Text("\(viewModel.partnerZodiac!.rawValue)")
                .font(.itim(size: 12))
                .foregroundColor(.black)
                .frame(height: 12)
            }
            .padding(.horizontal, 4)
            .padding(.vertical, 2)
            .background(getZodiacColor(viewModel.partnerZodiac!))
            .cornerRadius(12)
          }
        }
        .padding(.top, 4)
      }
      .frame(maxWidth: .infinity)
      .padding(.leading, 8)
      .padding(.trailing, 20)
    }
    
  }
  
  func startHeartAnimations() -> Void {
    
    withAnimation(Animation.easeIn(duration: 0.1)) {
      self.controller.heartScale = 0.5
    }
    
    withAnimation(Animation.easeIn(duration: 0.3).delay(0.1)) {
      self.controller.heartScale = 0.7
    }
    
    withAnimation(Animation.easeOut(duration: 0.4).delay(0.4)) {
      self.controller.heartScale = 1.0
    }
  }
  
  func ZodiacIcon(_ zodiac: Zodiac) -> some View {
    return Image(zodiac.rawValue)
      .resizable()
      .frame(width: 12, height: 12)
  }
  
  func getZodiacColor(_ zodiac: Zodiac) -> Color {
    switch zodiac {
    case .Aries:
      return Color.Aries
    case .Taurus:
      return Color.Taurus
    case .Gemini:
      return Color.Gemini
    case .Cancer:
      return Color.Cancer
    case .Leo:
      return Color.Leo
    case .Virgo:
      return Color.Virgo
    case .Libra:
      return Color.Libra
    case .Scorpio:
      return Color.Scorpio
    case .Sagittarius:
      return Color.Sagittarius
    case .Capricorn:
      return Color.Capricorn
    case .Aquarius:
      return Color.Aquarius
    case .Pisces:
      return Color.Pisces
    }
  }
}

struct DateTimeWidget: View {
  @ObservedObject var viewModel: HomeViewModel
  @EnvironmentObject var themes: ThemesController
  var body: some View {
    VStack(spacing: 4) {
      HStack {
        if themes.clockThemes == .normal {
          Text(viewModel.currentDate.toDateString())
            .font(.itim(size: 18))
            .foregroundColor(.white)
        } else if themes.clockThemes == .flip {
          FlipDate(size: 18, controller: viewModel.flipDateController)
        }
        Spacer()
        if themes.clockThemes == .normal {
          Text(viewModel.currentDate.toTimeString())
            .font(.itim(size:  18))
            .foregroundColor(.white)
        } else if themes.clockThemes == .flip {
          FlipClock(size: 18, controller: viewModel.flipClockController)
        }
      }
      .frame(maxWidth: .infinity)
      .frame(height: 24)
      
      ProgressWidget(date: viewModel.currentDate)
    }
    .frame(height: 34)
    .padding(.horizontal, 24)
    .onReceive(viewModel.timer) { date in
      DispatchQueue.main.async {
        viewModel.setCurrentDate(date: date)
      }
    }
  }
}

struct TogetherImageBackground: View {
  var body: some View {
    ZStack {
      Image("background")
        .resizable()
        .scaledToFill()
      
      Color.black.opacity(0.5)
    }
  }
}

struct HomeView_Previews: PreviewProvider {
  static var previews: some View {
    HomeView()
  }
}
