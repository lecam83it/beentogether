import SwiftUI
import Combine

class SharedTimer: ObservableObject {
  static let shared = SharedTimer()

  private var cancellable: AnyCancellable?
  
  var time: Timer.TimerPublisher = Timer.publish(every: 1.0, on: .current, in: .default)
  
  private init() {
    start()
  }
  
  @Published var currentDate = Date()
 
  private func start() {
    print(#function)
    cancellable = time.connect() as? AnyCancellable
  }
}

class BaseViewModel: ObservableObject {}
