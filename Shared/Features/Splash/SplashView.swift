import SwiftUI
import UIPilot

struct SplashView: View {
  @EnvironmentObject var navigation: UIPilot<AppRoute>
  let sharedPreferences = SharedPreferences.shared
  let repository = TogetherRepository.shared
  var body: some View {
    VStack {
    }
    .onAppear {
      if sharedPreferences.hasConfiguration {
        repository.getInfo {
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            navigation.push(.Home)
          }
        } onError: {
          repository.initialize()
          navigation.push(.Setup)
        }
      } else {
        repository.initialize()
        navigation.push(.Setup)
      }
    }
  }
}
