import SwiftUI

extension Color {
  static let lightGray = Color.init(hex: "D9D9D9")
  static let black54 = Color.init(hex: "212121")
  static let background = Color.init(hex: "F5F5F5")
  static let button = Color.init(hex: "F55441")
  //Zodiac
  static let Aries = Color.init(hex: "F6CCCC")
  static let Taurus = Color.init(hex: "D7F3F6")
  static let Gemini = Color.init(hex: "F5F3C1")
  static let Cancer = Color.init(hex: "DAEED3")
  static let Leo = Color.init(hex: "F5DEC6")
  static let Virgo = Color.init(hex: "DCCFE0")
  static let Libra = Color.init(hex: "F7DAE4")
  static let Scorpio = Color.init(hex: "DFE1F6")
  static let Sagittarius = Color.init(hex: "E4F6EC")
  static let Capricorn = Color.init(hex: "EFDFD6")
  static let Aquarius = Color.init(hex: "F2E0F4")
  static let Pisces = Color.init(hex: "E1D191")
  
  // Gender
  static let male = Color.init(hex: "F5F3C1")
  static let female = Color.init(hex: "F2E0F4")

  init(hex: String, with alpha: Double = 1.0) {
    let scanner = Scanner(string: hex)
    scanner.currentIndex = hex.startIndex
    var rgbValue: UInt64 = 0
    scanner.scanHexInt64(&rgbValue)

    let r = (rgbValue & 0xff0000) >> 16
    let g = (rgbValue & 0xff00) >> 8
    let b = rgbValue & 0xff

    self.init(
      red: CGFloat(r) / 0xff,
      green: CGFloat(g) / 0xff,
      blue: CGFloat(b) / 0xff,
      opacity: alpha
    )
  }
}

