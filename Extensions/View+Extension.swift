import SwiftUI

extension View {
  func placeholder<Content: View>(
    when visible: Bool,
    alignment: Alignment = .leading,
    @ViewBuilder placeholder: () -> Content
  ) -> some View {
    ZStack(alignment: alignment) {
      if visible {
        placeholder()
      }
      self
    }
  }
  
  func placeholder(
    _ hint: String,
    when:Bool,
    alignment: Alignment = .leading
  ) -> some View {
    placeholder(when: when, alignment: alignment) {
      Text(hint).foregroundColor(.gray)
    }
  }
}

extension Binding {
  func unwrappred<T>(defaultValue: T) -> Binding<T> where Value == Optional<T>  {
    Binding<T>(get: { self.wrappedValue ?? defaultValue }, set: { self.wrappedValue = $0 })
  }
}

extension UIApplication {
  func endEditing() {
    sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
  }
}
