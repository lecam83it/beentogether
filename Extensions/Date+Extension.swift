import SwiftUI

extension Date {
  func toDateString(with formatString: String = "dd/MM/yyyy") -> String {
    let dateFormated = DateFormatter()
    dateFormated.locale = Locale(identifier: "en_US_POSIX")
    dateFormated.dateFormat = formatString
    return dateFormated.string(from: self)
  }
  
  func toDateString() -> String {
    let dateFormated = DateFormatter()
    dateFormated.locale = Locale(identifier: "en_US_POSIX")
    dateFormated.setLocalizedDateFormatFromTemplate("MMM d, yyyy")
    return dateFormated.string(from: self)
  }
  
  func toTimeString() -> String {
    let dateFormated = DateFormatter()
    dateFormated.locale = Locale(identifier: "en_US_POSIX")
    dateFormated.setLocalizedDateFormatFromTemplate("HH:mm:ss")
    return dateFormated.string(from: self)
  }
  
  var timeInterval: TimeInterval {
    get {
      let hour = Calendar.current.component(.hour, from: self)
      let minute = Calendar.current.component(.minute, from: self)
      let second = Calendar.current.component(.second, from: self)
      return TimeInterval(hour * 3600 + minute * 60 + second)
    }
  }
  
  func diff (date: Date) -> Int? {
    let calendar = Calendar.current
    
    let to: Date = calendar.startOfDay(for: self)
    let from: Date = calendar.startOfDay(for: date)
    
    let components = calendar.dateComponents([.day], from: from, to: to)
    
    return components.day
  }
  
  var milliseconds: Int64 {
    Int64((self.timeIntervalSince1970 * 1000.0).rounded())
  }
  
  init(milliseconds: Int64) {
    self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
  }
}

extension DateFormatter {
  
  static var timeFormatter: DateFormatter {
    let formatter = DateFormatter()
    formatter.dateFormat = "HHmmss"
    return formatter
  }
  
  static var dateFormater: DateFormatter {
    let formatter = DateFormatter()
    formatter.dateFormat = "ddMMyyyy"
    return formatter
  }
}
