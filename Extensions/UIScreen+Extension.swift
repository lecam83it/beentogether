import UIKit
import SwiftUI

extension UIScreen {
  static let width = UIScreen.main.bounds.size.width
  static let height = UIScreen.main.bounds.size.height
  static let size = UIScreen.main.bounds.size
  static let statusBarHeight = UIApplication.statusBarHeight
//  static let homeIndicatorHeight = UIApplication.homeIndicatorHeight
  
//  static let safeHeight = height - statusBarHeight - homeIndicatorHeight
}


extension UIApplication {
  static var statusBarHeight: CGFloat {
    if #available(iOS 13.0, *) {
      let window = shared.connectedScenes.first as! UIWindowScene
      return window.statusBarManager?.statusBarFrame.height ?? 0
    } else {
      return shared.statusBarFrame.height
    }
  }
  
//  static var homeIndicatorHeight: CGFloat {
//    if #available(iOS 13.0, *) {
//      let window = shared.connectedScenes.first as! UIWindowScene
//      return window.keyWindow?.safeAreaInsets.bottom ?? 0
//    } else {
//      return shared.keyWindow?.safeAreaInsets.bottom ?? 0
//    }
//  }
}
